import os
import json
import requests
import urllib3
import math

from flask import Flask
from flask import Flask, render_template, request, jsonify

app = Flask(__name__, static_url_path='/static')

@app.route('/')
def Welcome():
	return render_template('index.htm')

@app.route('/score', methods=['GET', 'POST'])
def process_form_data():
	# Get the form data - result will contian all elements from the HTML form that was just submitted
	result = request.form

	#
	# The following lines are exactly the python code example form the Watson Machine Learning page
	#
	wml_credentials = {
		"apikey": "WL_TxmtlNwdp0ZULQU80ixUBZMpXJ770e9r6WMpgEDku",
		"iam_apikey_description": "Auto-generated for key bdef1d12-92b7-445a-9bc1-cd8f28301df1",
		"iam_apikey_name": "WMLwriter",
		"iam_role_crn": "crn:v1:bluemix:public:iam::::serviceRole:Writer",
		"iam_serviceid_crn": "crn:v1:bluemix:public:iam-identity::a/a126878c4ab9a074aa3e8091cf2081b8::serviceid:ServiceId-54a61468-c417-48ac-aaae-9e5ddf146a0d",
		"instance_id": "b818071d-1b42-4f3a-862e-b7b2335f1625",
		"url": "https://us-south.ml.cloud.ibm.com"
	}

	"""
	Obtain an IAM Token
	"""

	# Get an IAM token from IBM Cloud
	url     = "https://iam.bluemix.net/oidc/token"
	headers = { "Content-Type" : "application/x-www-form-urlencoded" }
	data    = "apikey={apikey}&grant_type=urn:ibm:params:oauth:grant-type:apikey".format(**wml_credentials)
	IBM_cloud_IAM_uid = "bx"
	IBM_cloud_IAM_pwd = "bx"
	response  = requests.post( url, headers=headers, data=data, auth=( IBM_cloud_IAM_uid, IBM_cloud_IAM_pwd ) )
	iam_token = response.json()["access_token"]

	scoring_endpoint = 'https://us-south.ml.cloud.ibm.com/v3/wml_instances/b818071d-1b42-4f3a-862e-b7b2335f1625/deployments/cbab3fd9-f13b-4abf-8c7b-d493f4923063/online'

	header = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + iam_token}

	#
	# Here the example code is slightly modified. The input from the form is send.
	# (!) No checks on data is done here.
	#
	payload_scoring = {"values": [[result["property_state"], result["distance_to_centre"], result["distance_to_metro"], result["mts2"]]]}

	response_scoring = requests.post(scoring_endpoint, json=payload_scoring, headers=header)
	json_response = response_scoring.json()

	json_response['exp_values'] = math.exp(json_response['values'][0][0]) 

	return jsonify( json_response ) 
	
port = os.getenv('VCAP_APP_PORT', '5000')
if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(port))
